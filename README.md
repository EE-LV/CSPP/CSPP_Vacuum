This Git submodule contains actor classes etc concerned with vacuum.

Related documents and information
=================================
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : [https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum](https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum)
- Documentation:
  - Project-Wiki: [https://github.com/HB-GSI/CSPP/wiki](https://github.com/HB-GSI/CSPP/wiki)
  - NI Actor Framework: [https://decibel.ni.com/content/groups/actor-framework-2011?view=overview](https://decibel.ni.com/content/groups/actor-framework-2011?view=overview)

GIT Submodules
==============
This package can be used as submodule.
- [https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum.git) It contains:
  - CSPP_MKS974B
  - CSPP_RVC
  - CSPP_TPG300
  
External Dependencies
-------------------
- [https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git): Definition of CS++Device ancestor classes
- [https://git.gsi.de/EE-LV/Drivers/MKS974B.git](https://git.gsi.de/EE-LV/Drivers/MKS974B.git): LabVIEW instrument driver
- [https://git.gsi.de/EE-LV/Drivers/RVC300.git](https://git.gsi.de/EE-LV/Drivers/RVC300.git): LabVIEW instrument driver
- [https://git.gsi.de/EE-LV/Drivers/TPG300.git](https://git.gsi.de/EE-LV/Drivers/TPG300.git): LabVIEW instrument driver


Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.